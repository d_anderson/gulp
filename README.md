# README #

This is a working, but in development gulp configuration.

### Current Working Features ###
* Error Handling
* JS Linting
* JS Minification
* SASS Compilation
* SASS Autoprefixing
* CSS Minification
* Gulp Watch Function
* Watching of JS,SASS,PHP files
* LiveReload
* Lazy loading of gulp plugins
* Cleaning of destination folders before file writing
* Nofitications of completed tasks as they are completing.


### Current Kind-of Working Features ###


### Future Features ###
* Image compression (Mike is working on this)
* Rsync/FTP syncing of folders
* PHP Linting (Mike is working on this)
* HTML Minification
* Splitting of Vendor and Developer JS files
* Compiling all Vendor files into a single js file



### How do I get set up? ###
**These instructions are to get this working on the Rosemont Dev server.**

1. Download current repo.
2. Add gulpfile.js and package.json to theme folder (/Volumes/Dev/website/wp-content/themes/your-theme/)
3. SSH into theme folder.
4. Run
```
#!javascript

sudo npm install
```

5. Add snippet to footer.php
```
#!javascript

<script src="http://10.10.10.11:35729/livereload.js"></script>
```

6. Initialize using
```
#!javascript

gulp watch
```