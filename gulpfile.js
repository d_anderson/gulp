/*******************************************************
********************* Gulp Modules *********************
********************************************************
**                                                    **
**                $ sudo npm install                  **
**      make sure you have the package.json file    **
**                                                    **
********************************************************
********************************************************/


var gulp        =  require('gulp');
var livereload  =  require('gulp-livereload');
var del         = require('del');
//var plugins     =  require('gulp-load-plugins')();

var gulpLoadPlugins = require('gulp-load-plugins');
var plugins = gulpLoadPlugins();


//Error Handling
var onError = function( err ) {
  console.log( 'An error occurred:', err.message );
  this.emit( 'end' );
}

// Jshint outputs any kind of javascript problems you might have
gulp.task( 'hint', function() {
  //Lint all JS files
  //return gulp.src( './js/**/*.js' )
  //Lint only root JS folder
  return gulp.src( './js/*.js' )
  .pipe(plugins.jshint())
  .pipe(plugins.jshint.reporter('jshint-stylish'))
  .pipe(plugins.notify("JS hinting complete"));
});

//Minify JS
gulp.task( 'scripts',  ['clean-scripts'],  function() {
  return gulp.src( './js/scripts.js' )
    .pipe(plugins.uglify() )
    .pipe(plugins.rename( { suffix: '.min' } ) )
    .pipe(gulp.dest( './min/js/' ) )
    .pipe(plugins.notify("JS has been minified."))
    .pipe(livereload() );
} );

//Getting Sassy
gulp.task( 'sassy', ['clean-sass'], function() {
  return gulp.src( './sass/**/*.scss' )
  .pipe(plugins.plumber( { errorHandler: onError }))
  .pipe(plugins.sass())
  .pipe(plugins.autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1'))
  .pipe(gulp.dest( '.' ))
  .pipe(plugins.notify("SASS has been compiled."))
  .pipe(plugins.minifyCss())
  .pipe(plugins.rename({suffix: '.min' }))
  .pipe(gulp.dest('./min/css/'))
  .pipe(plugins.notify("SASS has been minified."))
  .pipe(livereload());
} );

//We need more lemon pledge
gulp.task('clean-scripts', function(cb) {
    del(['./min/js'], cb)
});

gulp.task('clean-sass', function(cb) {
    del(['./min/css', './style.css'], cb)
});

//NSAing your files
gulp.task( 'watch', function() {
  livereload.listen("host: 10.10.10.11");
  gulp.watch('./js/scripts.js', [ 'hint', 'scripts' ] )
  gulp.watch('./sass/**/*.scss', [ 'sassy' ] );
  gulp.watch('./**/*.php' ).on( 'change', function( file ) {
  livereload.changed( file );
  } );
} );

gulp.task( 'default', ['clean-scripts', 'hint', 'scripts', 'clean-sass', 'sassy'], function() {});